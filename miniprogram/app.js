//app.js
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        // env: 'my-env-id',
        // env: 'gxxc-d64e32',
        env: 'banjizhushou-1g2omcl25bbb54c1',
        traceUser: true,
      })
      this.db = wx.cloud.database(); //云数据库初始化
      this.getOpenid();
    }
    // this.globalData = {}l
  },
  // 获取用户openid
  getOpenid: function() {
    var app = this;
    var openidStor = wx.getStorageSync('openid');
    if (openidStor) {
    console.log('本地获取openid:' + openidStor);
    app.globalData.openid = openidStor;
    app._getUserInfo();
    } else {
    wx.cloud.callFunction({
      name: 'login',
      complete: res => {
      console.log('云函数获取到的openid: ', res.result.openid)
      var openid = res.result.openid;
      wx.setStorageSync('openid', openid)
      app.globalData.openid = openid;
      app._getUserInfo();
      }
    })
    }
  },
    // 获取用户信息，如果用户没有授权，就获取不到
  _getUserInfo: function() {
    var app = this;
    wx.getUserInfo({ //从网络获取最新用户信息
    success: function(res) {
      var user = res.userInfo;
      app.globalData.userInfo = user;
      console.log('请求获取user成功',user)
      app._saveUserInfo(user);
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      if (app.userInfoReadyCallback) {
      app.userInfoReadyCallback(res)
      }
    },
    fail: function(res) { //请求网络失败时，再读本地数据
      console.log('请求获取user失败')
      var userStor = wx.getStorageSync('user');
      if (userStor) {
      app.globalData.userInfo = userStor;
      }
    }
    })
  },
    // 保存userinfo
  _saveUserInfo: function(user) {
    this.globalData.userInfo = user;
    this._getMyUserInfo(user);
  },
  //获取自己后台的user信息
  _getMyUserInfo(user) {
    let app = this
    user.openid = wx.getStorageSync('openid');
    var user = JSON.stringify(user)
    wx.request({
    url: app.globalData.baseUrl + '/user',
    data: user,
    method: 'POST',
    success: function(res) {
      console.log("授权成功", res)
      if(res.data.code==200){
        //缓存到sd卡里
        console.log("微信授权登录成功");
      }
    },
    fail:function(){
      console.log("微信授权登录失败");
    }
    })
  },
  //创建towxml对象，供小程序页面使用
  globalData: {
    userInfo: null,
    openid: null,
    baseUrl: 'http://localhost:8088', //本地调试
    grade_id: 0,
    roleId:0
  },
})
