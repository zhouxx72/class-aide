// miniprogram/pages/chuangjian/chuangjian.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    grade:{
      college:'',
      grade:'',
      major:'',
      gradeName:'',
      gradeImg:'',
      password:''
    }
  },
  college(e){
    let that=this;
    that.data.grade.college = e.detail.value
  },
  grade(e){
    let that=this;
    that.data.grade.grade = e.detail.value
  },
  major(e){
    let that=this;
    that.data.grade.major = e.detail.value
  },
  gradeName(e){
    let that=this;
    that.data.grade.gradeName = e.detail.value
  },
  // grade_img(e){
  //   let that=this;
  //   that.data.grade.grade_img = e.detail.value
  // },
  password(e){
    let that=this;
    that.data.grade.password = e.detail.value
  },
  //选择图片
  chooseImage: function () {
    let that=this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: res => {
        let tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: app.globalData.baseUrl +'/album/upload',
          filePath: tempFilePaths[0],
          name: 'file',
          success(res) {
            console.log("上传图片", res.data)
            let item='grade.gradeImg';
            that.setData({
              [item]: res.data
            })
          }
        })
      }
    })
  },
  //创建班级
  btn(){
    let that=this;
    let grade=that.data.grade;
    let openid = wx.getStorageSync('openid');
    console.log("创建班级openid",openid)
    wx.request({
      url: app.globalData.baseUrl + '/grade?openid='+openid,
      data: grade,openid,
      method: "POST",
      success: function (res) {
        if(res.data.code){
          wx.showToast({
            title: '创建班级成功',
            icon: 'success',
            duration: 2000//持续的时间
          })
          wx.switchTab({
            url: '../me/me'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})