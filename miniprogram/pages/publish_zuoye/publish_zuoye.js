// pages/publish_zuoye/publish_zuoye.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    assignment:{}
  },
  //作业内容
  zyContent(e) {
    let that = this;
    that.data.assignment.assignmentContent = e.detail.value;
  },
  //作业标题
  zyTitle(e) {
    let that = this;
    that.data.assignment.assignmentTitle = e.detail.value
  },
  //创建作业信息
  cjzy(){
    let that = this;
    let openid = wx.getStorageSync('openid');
    let gradeId = app.globalData.grade_id;
    let assignment = that.data.assignment;
    wx.request({
      url: app.globalData.baseUrl + '/assignment?gradeId=' + gradeId + '&openid=' + openid,
      data: assignment,
      method: "POST",
      success: function (res) {
        console.log("创建作业信息", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000//持续的时间
          })
          wx.navigateTo({
            url: '../zuoye/zuoye'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})