// pages/course/course.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index:0,
    course: ['语文','数学','英语','美术','生物','化学'], //常用课程
    hiddenmodalput: true,
    section: [1, 2, 3, 4, 5, 6, 7, 8],
    colorArrays: ["#85B8CF", "#90C652", "#D8AA5A", "#FC9F9D", "#0A9A84", "#61BC69", "#12AEF3", "#E29AAD","#85B8CF", "#90C652"],
    //xqj代表周几  skjc代表占用课时 kcmc课程名称
    wlist: [
      { "xqj": 1, "skjc": 1, "skcd": 1, "kcmc": "数学"  },
      { "xqj": 1, "skjc": 2, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 3, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 4, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 5, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 6, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 7, "skcd": 1, "kcmc": "" },
      { "xqj": 1, "skjc": 8, "skcd": 1, "kcmc": "" },
      { "xqj": 2, "skjc": 1, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 2, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 3, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 4, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 5, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 6, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 7, "skcd": 1, "kcmc": "数学" },
      { "xqj": 2, "skjc": 8, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 1, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 2, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 3, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 4, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 5, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 6, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 7, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 8, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 1, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 2, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 3, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 4, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 5, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 6, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 7, "skcd": 1, "kcmc": "数学" },
      { "xqj": 4, "skjc": 8, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 1, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 2, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 3, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 4, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 5, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 6, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 7, "skcd": 1, "kcmc": "数学" },
      { "xqj": 5, "skjc": 8, "skcd": 1, "kcmc": "数学" },
      { "xqj": 6, "skjc": 1, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 2, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 3, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 4, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 5, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 6, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 7, "skcd": 1, "kcmc": "" },
      { "xqj": 6, "skjc": 8, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 1, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 2, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 3, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 4, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 5, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 6, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 7, "skcd": 1, "kcmc": "" },
      { "xqj": 7, "skjc": 8, "skcd": 1, "kcmc": "" },
    ],
    item:{},
    courseName:'',
    roleId: 0
  },
  //增加节数
  add(){
    let that=this;
    let add=that.data.section.length;
    if(add>9){
      wx.showToast({
        title: '最多为10节课',
        icon: 'none',
        duration: 2000//持续的时间
      });
      return;
    }
    let section=that.data.section;
    let value=add+1;
    let wlist=that.data.wlist;
    for (let index = 0; index < 7; index++) {
        let work={
          'xqj':index+1,
          'skjc':value,
          'skcd':1,
          'kcmc':''
        }
        let num=(index+1)*7+index+1+index;
        wlist.splice(num,0,work)
    }
    section.push(value);
    that.setData({
      section,wlist
    })
  },
  //减少节数
  reduce(){
    let that = this;
    let indexs = that.data.section.length;
    if(indexs<9){
      wx.showToast({
        title: '至少为8节课',
        icon: 'none',
        duration: 2000//持续的时间
      });
      return;
    }
    let section = that.data.section;
    let value=indexs-1;
    let wlist=that.data.wlist;
    for (let index = 7; index > -1; index--) {
        let work={
          'xqj':index+1,
          'skjc':indexs,
          'skcd':1,
          'kcmc':''
        }
        let num=(index+1)*7+index+1+index;
        wlist.splice(num,1)
    }
    section.splice(value,1);
    that.setData({
      section,wlist
    })
  },
  //添加课程
  showCardView(e){
    let that=this;
    let item = e.currentTarget.dataset.index
    let courseName = e.currentTarget.dataset.item.kcmc
    console.log("添加课程", item)
    console.log("课程名称", courseName)
    //打开添加课程的模态框
    that.setData({
      hiddenmodalput: !this.data.hiddenmodalput,
      item:item,
      courseName: courseName
    })
  },
  //常用课程事件
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  //保存课表
  saveCource(){
    let that=this;
    let course=that.data.wlist;
    let gradeId = app.globalData.grade_id;
    console.log("保存课程表", course)
    console.log("班级id", gradeId)
    wx.request({
      url: app.globalData.baseUrl + '/course?gradeId=' + gradeId,
      data: {'course':JSON.stringify(course)},
      method: "POST",
      success: function (res) {
        console.log("老师加入", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 2000//持续的时间
          })
        } 
      }
    })
  },
  //取消按钮  
  cancel: function () {
    this.setData({
      hiddenmodalput: true
    });
  },
  //确认  
  confirm: function () {
    let that=this;
    let item=that.data.item;
    let wlist=that.data.wlist;
    wlist[item].kcmc= that.data.courseName
    console.log("添加课程",item)
    that.setData({
      hiddenmodalput: true,
      wlist: wlist
    })
  },
  //手动输入课程
  inputcourse(e){
    let that = this;
    that.data.courseName = e.detail.value
  },
  //查课程列表
  getCourse(){
    let that=this;
    let gradeId = app.globalData.grade_id;
    if(gradeId==undefined){
      return
    }
    wx.request({
      url: app.globalData.baseUrl + '/course?gradeId=' + gradeId,
      method: "GET",
      success: function (res) {
        console.log("查课程列表", res)
        if (res.data.code == 200 && res.data.data !=undefined) { 
          var course = res.data.data.curriculum;
          console.log("课程", course)
          that.setData({
            wlist: JSON.parse(course)
          })
        } 
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.setData({
      roleId: app.globalData.roleId
    })
    that.getCourse()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})