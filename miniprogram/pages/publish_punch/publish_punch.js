// pages/publish_punch/publish_punch.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickName: '',
    regime:{},
    length: 0,
    checked: false,
    checked1: false,
    startTime: '8:00',
    endTime: '22:00',
    date:'请选择'
  },
  //设置打卡时间段
  checkedTap1: function (e) {
    console.log(`Switch样式点击后是否选中：`, e.detail.value)
    let that=this;
    that.setData({
      checked1: e.detail.value
    })
  },
  //开始时间
  bindStartTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },
  //结束时间
  bindEndTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      endTime: e.detail.value
    })
  },
  //设置打卡截止日期
  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value
    })
  },
  //制度内容
  punchContent(e) {
    let that = this;
    that.data.regime.regimeContent = e.detail.value;
  },
  //制度标题
  punchTitle(e) {
    let that = this;
    that.data.regime.regimeTitle = e.detail.value
  },
  //创建打卡
  createPunch(){
    let that=this;
    let openid = wx.getStorageSync('openid');
    let gradeId = app.globalData.grade_id;
    that.data.regime.regimeStarttime = that.data.startTime;
    that.data.regime.regimeEndtime = that.data.endTime;
    that.data.regime.regimeDate = Date.parse(that.data.date);
    let regime=that.data.regime;
    wx.request({
      url: app.globalData.baseUrl + '/regime?gradeId=' + gradeId + '&openid=' + openid,
      data: regime,
      method: "POST",
      success: function (res) {
        console.log("创建制度", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000//持续的时间
          })
          wx.navigateTo({
            url: '../punch/punch'
          })
        } 
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})