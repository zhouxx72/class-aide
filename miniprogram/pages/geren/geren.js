// miniprogram/pages/geren/geren.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowUserName: false,
    userInfo: null,
  },
  // button获取用户信息
  onGotUserInfo: function(e) {
    console.log("button获取用户信息",e)
    if (e.detail.userInfo) {
    var user = e.detail.userInfo;
    user.openid = app.globalData.openid;
    this.setData({
      isShowUserName: true,
      userInfo: e.detail.userInfo,
    })
      app._getMyUserInfo(user);
    } else {
      console.log("没获取到用户信息，授权失败")
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var user = app.globalData.userInfo;
    if (user) {
     that.setData({
      isShowUserName: true,
      userInfo: user,
     })
    } else {
     // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
     // 所以此处加入 callback 以防止这种情况
     app.userInfoReadyCallback = res => {
      that.setData({
       userInfo: res.userInfo,
       isShowUserName: true
      })
     }
    }
  },
  //跳转到切换班级页面
  qiehuan:function(e){
    wx.navigateTo({
      url: '../qiehuan/qiehuan'
    })
  },
  //跳转到个人信息页面
  gerenxx:function(e){
    wx.navigateTo({
      url: '../xinxi/xinxi?id='+e.currentTarget.dataset.id
    })
  },
  //跳转到创建班级页面
   chuangjianjiaru:function(e){
     wx.navigateTo({
       url: '../chuangjian_jiaru/chuangjian_jiaru?id='+e.currentTarget.dataset.id
     })
   },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    console.log("个人show", options)
    var user = app.globalData.userInfo;
    if (user) {
     this.setData({
      isShowUserName: true,
      userInfo: user,
     })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})