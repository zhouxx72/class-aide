// pages/picture/picture.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{ 'photoUrl': '../../images/相册.PNG' }],
    album_id:''
  },
  //查询相册详情
  getData: async function () {
    let that = this;
    let albumId = that.data.album_id;
    console.log("相册id", albumId )
    wx.request({
      url: app.globalData.baseUrl + '/album/detailsList',
      data: { "albumId": albumId },
      method: 'GET',
      success: function (res) {
        console.log("查询相册详情", res)
        if (res.data.code == 200) {
          that.setData({
            list: res.data.data
          })
        }
      }
    })
  },

  upload_image: function (res) {
    let that = this;
    let openid = wx.getStorageSync('openid');
    let grade_id = app.globalData.grade_id;
    let imageUrl = that.data.imageUrl;
    let album_id = that.data.album_id;
    wx.chooseImage({
      count: 9,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: (res) => {
        const tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: app.globalData.baseUrl + '/album/uploadPhoto',
          filePath: tempFilePaths[0],
          name: 'file',
          formData: {
            openid: openid,
            gradeId: grade_id,
            albumId: album_id
          },
          success(res) {
            console.log("上传图片", res.data)
            that.getData()
          }
        })
      }
    })
  },

  preview_image: function (res) {
    let imageUrl = res.currentTarget.dataset.imageurl
    let urls = []
    for (let i = 0; i < this.data.list.length; i++) {
      urls.push(this.data.list[i].image_url)
    }
    wx.previewImage({
      current: imageUrl,
      urls: urls
    })
  },

  onReachBottom: function () {
    console.log('1')
    !this.data.isEndOfList && this.getData()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let item = JSON.parse(options.item)
    wx.setNavigationBarTitle({
      title: item.albumName
    })
    this.setData({
      album_id: item.albumId
    })
    this.getData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})