// pages/create/create.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    photosNew: [],
    imageUrl:''
  },
  //上传图片
  upload_image: function () {
    if (this.data.title === undefined || this.data.photosNew[0] === undefined) {
      wx.showToast({
        title: '上传信息不能为空!',
        icon: 'loading',
        duration: 500
      })
    } else {
      //保存
      console.log("图片名称",this.data.photosNew[0])
      console.log("标题", this.data.title)
      console.log("图片路径", this.data.imageUrl)
      this.uploadPhoto()
    }
  },
  //选择图片
  chooseImage: function () {
    let that=this;
    const items = this.data.photosNew
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: res => {
        let tempFilePaths = res.tempFilePaths
        for (const tempFilePath of tempFilePaths) {
          items.push({
            src: tempFilePath
          })
        }
        this.setData({
          photosNew: items,
        })
        wx.uploadFile({
          url: app.globalData.baseUrl +'/album/upload',
          filePath: tempFilePaths[0],
          name: 'file',
          success(res) {
            console.log("上传图片", res.data)
            that.setData({
              imageUrl: res.data
            })
          }
        })
      }
    })
  },
  create_title: function (event) {
    this.setData({
      title: event.detail.value
    })
  },
  // 保存到数据库中
  uploadPhoto(filePath) {
    let that = this;
    let openid = wx.getStorageSync('openid');
    let title = that.data.title;
    let imageUrl = that.data.imageUrl;
    let gradeId = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/album?openid=' + openid + '&gradeId=' + gradeId,
      data: { "albumName": title, "albumUrl": imageUrl },
      method: 'POST',
      success: function (res) {
        console.log("新建相册", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: '上传成功!',
            icon: 'success',
            duration: 2000//持续的时间
          })
          wx.navigateTo({
            url: '../album/album'
          })
        }
      }
    })
  },

  // 预览图片
  previewImage(e) {
    const current = e.target.dataset.src
    const photos = this.data.photosNew.map(photo => photo.src)
    wx.previewImage({
      current: current.src,
      urls: photos
    })
  },

  // 删除图片
  cancel(e) {
    const index = e.currentTarget.dataset.index
    const photos = this.data.photosNew.filter((p, idx) => idx !== index)
    this.setData({
      photosNew: photos
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})