// miniprogram/pages/xinxi/xinxi.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user:{
      userName:"张三",
      gender:1,
      userPhone:"1599991999",
      openid:''
    }
  },
  userName(e){
    let that=this;
    that.data.user.userName = e.detail.value
  },
  gender(e){
    let that=this;
    that.data.user.gender = e.detail.value
  },
  userPhone(e){
    let that=this;
    that.data.user.userPhone = e.detail.value
  },
  number(e){
    let that=this;
    that.data.user.number = e.detail.value
  },
  saveInfo(){
    let that=this;
    let openid = wx.getStorageSync('openid');
    that.data.user.openid=openid
    var user = JSON.stringify(that.data.user)
    wx.request({
    url: app.globalData.baseUrl + '/user',
    data: user,
    method: 'POST',
    success: function(res) {
      console.log("保存成功", res)
      if(res.data.code==200){
        wx.showToast({
          title: '保存成功!',
          icon: 'success',
          duration: 5000,//持续的时间    
          success: function() {
            setTimeout(function() {
              //要延时执行的代码
              wx.navigateBack({
                url: '../geren/geren'
              })
            }, 5000) //延迟时间
          }, 
        })
      }
    }
    })
  },
  //查询用户信息
  selectUser(){
    let that=this;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/user/selectInfo?openid='+openid,
      method: 'GET',
      success: function(res) {
        console.log("用户信息查询", res)
        if(res.data.code==200&&res.data.data!=undefined){
           that.setData({
             user: res.data.data
           })
        }
      }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.selectUser();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})