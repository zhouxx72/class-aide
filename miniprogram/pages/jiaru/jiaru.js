// miniprogram/pages/jiaru/jiaru.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //亲属关系对应角色
    role: [],
    index:0,
    showView: true,
    teacher:'white',
    jz:'blue',
    teachers:{
      password:'',
      userName:'',
      userPhone:'',
      roleId: ''
    },
    parent:{
      password: '',
      userName: '',
      userPhone: '',
      roleId:''
    }
  },
  //家长表单切换
  onChangeShowState: function () {
    let that=this;
    let teacher = that.data.teacher;
    let jz = that.data.jz;
    that.setData({
      showView: (!this.data.showView),
      teacher: jz,
      jz: teacher,
    })
  },
  //老师表单切换
  onChangeTeacherState(){
    let that = this;
    let jz = that.data.jz;
    let teacher = that.data.teacher;
    that.setData({
      showView: (!this.data.showView),
      teacher: jz,
      jz: teacher,
    })
  },
  //获取亲属关系对应值
  bindPickerChange: function (e) {
    let that=this;
    let index=e.detail.value
    var roleId = this.data.role[index].roleId;
    console.log('picker发送选择改变，携带值为', roleId)
    that.data.parent.roleId=roleId;
    this.setData({
      index: e.detail.value
    })
  },
  //老师表单数据
  password(e) {
    let that = this;
    that.data.teachers.password = e.detail.value
  },
  userName(e) {
    let that = this;
    that.data.teachers.userName = e.detail.value
  },
  userPhone(e) {
    let that = this;
    that.data.teachers.userPhone = e.detail.value
  },
  //老师加入班级
  teacherAddGrade(){
    let that = this;
    that.data.teachers.roleId=1;
    let teacherValue=that.data.teachers;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/user/updateUserGrade?openid='+openid,
      data: teacherValue,
      method: "POST",
      success: function (res) {
        console.log("老师加入", res)
        if (res.data.code == 200) {
          wx.switchTab({
            url: '../me/me'
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
  //家长表单数据
  jzpassword(e){
    let that = this;
    that.data.parent.password = e.detail.value
  },
  jzuserName(e){
    let that = this;
    that.data.parent.userName = e.detail.value
  },
  jzuserPhone(e){
    let that = this;
    that.data.parent.userPhone = e.detail.value
  },
  //家长加入班级
  jzAddGrade(){
    let that = this;
    let parentValue=that.data.parent;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/user/updateUserGrade?openid='+openid,
      data: parentValue,
      method: "POST",
      success: function (res) {
        console.log("老师加入", res)
        if (res.data.code == 200) {
          wx.switchTab({
            url: '../me/me'
          })
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
  //获取亲属关系对应的所有角色
  getRoleList(){
    let that=this;
    wx.request({
      url: app.globalData.baseUrl + '/grade/roleList',
      method: "GET",
      success: function (res) {
        console.log("获取所有角色", res)
        if (res.data.code == 200) {
          that.setData({
            role: res.data.data
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 生命周期函数--监听页面加载
    let that = this;
    that.getRoleList();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})