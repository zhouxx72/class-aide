// miniprogram/pages/qiehuan/qiehuan.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     
  },
  //获取已加入班级列表
  getGradeList(){
    let that=this;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/grade/joinList',
      method: "GET",
      data: {"openid": openid },
      success: function (res) {
        console.log("获取已加入班级列表", res.data.data)
        if (res.data.code==200) {
          that.setData({
            list: res.data.data
          })
        }
      }
    })
  },
  //切换班级 异步执行
  qiehuan(e){
    let that = this;
    let grade_id = e.currentTarget.dataset.id;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/grade/editGradeIsShow?openid='+openid,
      method: "POST",
      data: JSON.stringify({"gradeId": grade_id,"isShow":"1"}),
      success: function (res) {
        console.log("获取已加入班级列表", res)
        if (res.data.code == 200) {
          wx.switchTab({
            url: '../me/me'
          })
          app.globalData.grade_id = grade_id;
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.getGradeList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})