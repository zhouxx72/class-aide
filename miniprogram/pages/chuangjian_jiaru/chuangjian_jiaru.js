// miniprogram/pages/chuangjian_jiaru/chuangjian_jiaru.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  //跳转到创建班级页面
  chuangjian:function(e){
    wx.navigateTo({
      url: '../chuangjian/chuangjian'
    })
  },
  //跳转到切换班级页面
  qiehuan:function(e){
    wx.navigateTo({
      url: '../qiehuan/qiehuan'
    })
  },
  //加入已有班级
  jiaru(){
    wx.navigateTo({
      url: '../jiaru/jiaru'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})