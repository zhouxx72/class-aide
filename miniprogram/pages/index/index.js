// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    image_url:'../../images/相册.PNG'
    // image_url: 'D:/uploadPath/wxa954a3aa237c266b.o6zAJs9Mfz1aslA-jzA_ElG3SLOI.FolYXbKOBNoy86f2d40f4b30929b7ff4c51233a389f0.jpg'
  },
  nav_album: function (res) {
    wx.navigateTo({
      url: '../album/album'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})