// pages/zuoye/zuoye.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [
      {
      'avatar_url':'../../images/作业.PNG',
      'nick_name':'周星星',
      'assignment_title':'标题',
      'assignment_content':'发布的作业'
      }
      ],
    roleId: 0,
    toDayAssignment: 0,
  },
  //新建作业记录
  cjassignment(){
    wx.navigateTo({
      url: '../publish_zuoye/publish_zuoye'
    })
  },
  //完成作业
  assignment(e){
    let that=this;
    let openid = wx.getStorageSync('openid');
    let assignment_id = e.currentTarget.dataset.id;
    wx.request({
      url: app.globalData.baseUrl + '/assignment/addAssignment',
      data: { "assignmentId": assignment_id, "openid": openid},
      method: 'POST',
      success: function (res) {
        console.log("立即打卡", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: '完成作业!',
            icon: 'success',
            duration: 2000//持续的时间
          })
          wx.navigateTo({
            url: '../me/me'
          })
        }
      }
    })
  },
  //获取作业信息列表
  getAssignment() {
    let that = this;
    let gradeId = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/assignment/list',
      data: { 'gradeId': gradeId },
      method: "GET",
      success: function (res) {
        console.log("获取正在进行制度列表", res)
        if (res.data.code == 200&&res.data.data != undefined) {
          that.setData({
            list: res.data.data
          })
        }
      }
    })
  },
  //查询是否已完成作业
  getToDayAssignment() {
    let that = this;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/assignment/toDayAssignment',
      data: { "openid": openid },
      method: 'GET',
      success: function (res) {
        console.log("查询当天是否打卡", res)
        if (res.data.code == 200) {
          if (res.data.data != null && res.data.data != undefined) {
            that.setData({
              toDayAssignment: 1
            })
          }
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.getAssignment()
    that.getToDayAssignment()
    //获取全局角色id
    that.setData({
      roleId:app.globalData.roleId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})