// miniprogram/pages/xiaoxi/xiaoxi.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickName:'',
    inform_content:'',
    inform_title:'',
    length:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    that.getUserInfo()
  },
  //获取用户信息
  getUserInfo(){
    let that = this;
    that.setData({
      nickName: app.globalData.userInfo.nickName
    })
  },
  xiugaineirong(e){
    let that = this;
    that.data.inform_content = e.detail.value;
    that.data.length = e.detail.value.length;
  },
  informTitle(e){
    let that = this;
    that.data.inform_title = e.detail.value
  },
  //发布通知
  release(){
    let that = this;
    let openid = wx.getStorageSync('openid');
    var inform_title=that.data.inform_title;
    var length = that.data.length;
    var inform_content=that.data.inform_content;
    let gradeId = app.globalData.grade_id;
    console.log("发布通知", length)
    if(length<=0){
      wx.showToast({
        title: '发布通知不能为空',
        icon: 'none',
        duration: 2000//持续的时间
      });
      return;
    }
    if (inform_title==''){
      inform_title = that.data.nickName+'老师发布的通知';
    }
    wx.request({
      url: app.globalData.baseUrl + '/inform',
      data: {
        'informTitle': inform_title, 'informContent': inform_content, 'openid': openid,'gradeId':gradeId},
      method: "POST",
      success: function (res) {
        console.log("老师加入", res)
        if (res.data.code == 200) {
          wx.switchTab({
            url: '../me/me'
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})