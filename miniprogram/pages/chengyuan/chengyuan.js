// miniprogram/pages/chengyuan/chengyuan.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  //跳转到个人信息页面
  gerenxx:function(e){
    wx.navigateTo({
      url: '../xinxi/xinxi?id='+e.currentTarget.dataset.id
    })
  },
  gradeMemberList(){
    let that=this;
    let grade_id=app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/grade/gradeMemberList',
      data: { "gradeId":grade_id},
      method: "GET",
      success: function (res) {
        console.log("获取当前选中班级", res)
        if (res.data.code == 200) {
          that.setData({
            list: res.data.data
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.gradeMemberList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})