// pages/punch/punch.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    currentTab: 0,
    list:[],
    endlist:[],
    roleId: 0,
    toDayPunch:0
  },
  //进入发布打卡页面
  cjpunch(){
    wx.navigateTo({
      url: '../publish_punch/publish_punch'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    /**
     * 获取当前设备的宽高
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
  },
  //  tab切换逻辑
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
  },
  //获取已完结制度列表
  getRegimeEnd() {
    let that = this;
    let gradeId = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/regime/endList',
      data: { 'gradeId': gradeId },
      method: "GET",
      success: function (res) {
        console.log("获取已完结制度列表", res)
        if (res.data.code == 200) {
          that.setData({
            endlist:res.data.data
          })
        }
      }
    })
  },
  //获取正在进行制度列表
  getRegimeing() {
    let that = this;
    let gradeId = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/regime/list',
      data: { 'gradeId': gradeId },
      method: "GET",
      success: function (res) {
        console.log("获取正在进行制度列表", res)
        if (res.data.code == 200) {
          that.setData({
            list:res.data.data
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  //立即打卡
  lijipunch:function(e){
    let that=this;
    let openid = wx.getStorageSync('openid');
    let regime_id = e.currentTarget.dataset.id;
    let grade_id = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/regime/addPunch',
      data: { "regimeId": regime_id, "openid": openid, "gradeId": grade_id},
      method: 'POST',
      success: function (res) {
        console.log("立即打卡", res)
        if (res.data.code == 200) {
          wx.showToast({
            title: '今日打卡成功!',
            icon: 'success',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
  //查询当前用户角色
  getUserRole() {
    let that = this;
    let roleId=app.globalData.roleId;
    that.setData({
      roleId: roleId
    })
  },
  //查询当天是否打卡
  getToDayPunch(){
    let that=this;
    let openid = wx.getStorageSync('openid');
    let grade_id = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/regime/toDayPunch',
      data: { "openid": openid, "gradeId": grade_id },
      method: 'GET',
      success: function (res) {
        console.log("查询当天是否打卡", res)
        if (res.data.code == 200) {
          if (res.data.data != null && res.data.data != undefined) {
            that.setData({
              toDayPunch: 1
            })
          }
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.getUserRole()
    that.getToDayPunch()
    that.getRegimeing();
    that.getRegimeEnd();  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})