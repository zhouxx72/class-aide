// pages/album/album.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{ 'albumName': '我的', 'albumUrl': '../../images/相册.PNG' }],
  },
  //获取数据
  getData(){
    let that = this;
    let gradeId = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/album/list',
      data: { "gradeId": gradeId},
      method: 'GET',
      success: function (res) {
        console.log("查询相册列表", res)
        if (res.data.code == 200) {
          that.setData({
            list: res.data.data
          })
        }
      }
    })
  },
  //查询相册详情
  nav_picture: function (res) {
    let item = res.currentTarget.dataset.item
    wx.navigateTo({
      url: '../picture/picture?item=' + JSON.stringify(item),
    })
  },
  //创建新相册
  nav_create_album: function (res) {
    wx.navigateTo({
      url: '../create/create',
    })
  },

  onReachBottom: function (res) {
    !this.data.isEndOfList&&this.getData()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})