// miniprogram/pages/tongzhi/tongzhi.js
var app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleId:0,
    informList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  //跳转到发布信息页面
   tongzhi:function(e){
     wx.navigateTo({
       url: '../xiaoxi/xiaoxi?item=' + JSON.stringify(e.currentTarget.dataset.id)
     })
   },
  //跳转到通知信息页面
  chankantongzhi:function(e){
    wx.navigateTo({
      url: '../chakan_tongzhi/chankan_tongzhi?item=' + JSON.stringify(e.currentTarget.dataset.id)
    })
  },
  //查询当前用户角色
  getUserRole() {
    let that = this;
    let openid = wx.getStorageSync('openid');
    let grade_id = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/user/currentRole',
      data: { "openid": openid, "grade_id": grade_id },
      method: 'GET',
      success: function (res) {
        console.log("用户当前角色", res)
          if (res.data.code == 200&& res.data.data != undefined) {
            let roleId=res.data.data.role_id;
            app.globalData.roleId=roleId
            that.setData({
              roleId: roleId
            })
        }
      }
    })
  },
  //查询当前用户所在班级通知
  getInform(){
    let that = this;
    let grade_id = app.globalData.grade_id;
    wx.request({
      url: app.globalData.baseUrl + '/inform/list',
      data: { "gradeId": grade_id },
      method: 'GET',
      success: function (res) {
        console.log("用户当前所在班级通知", res)
        if (res.data.code == 200&&res.data.data!=undefined) {
          that.setData({
            informList: res.data.data
          })
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    that.getUserRole();
    that.getInform();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})