// miniprogram/pages/me/me.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tempfilepath: null,
    list:{}
  },
  //跳转到作业页面
  zuoye(){
    wx.navigateTo({
      url: '../zuoye/zuoye'
    })
  },
  //跳转到班级课程页面
  course(){
    wx.navigateTo({
      url: '../course/course'
    })
  },
  //跳转到班级打卡页面
  punch(){
    wx.navigateTo({
      url: '../punch/punch'
    })
  },
  //跳转到信息页面
  qiehuan:function(){
    wx.navigateTo({
      url: '../qiehuan/qiehuan'
    })
  },
  //跳转到班级信息页面
  banjixinxi:function(){
    let that=this;
    let list=that.data.list;
    wx.navigateTo({
      url: '../banji_xixi/banji_xinxi?list='+JSON.stringify(list)
    })
  },
  //获取当前选择班级
  getGradeShow(){
    let that = this;
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/grade/myGradeList',
      data: {"openid":openid},
      method: "GET",
      success: function (res) {
        console.log("获取当前选中班级", res)
        if (res.data.code == 200&& res.data.data != undefined) {
          let grade_id=res.data.data.grade_id;
          app.globalData.grade_id=grade_id
          that.setData({
            list: res.data.data
          })
          that.getUserRole(grade_id);
        }
      }
    })
  },
  bjalbum(){
    wx.navigateTo({
      url: '../index/index'
    })
  },
  // bjalbum(){
    // var that = this
    // wx.showActionSheet({
    //   itemList: ['拍照', '从相册中选择'],
    //   success(res) {
    //     console.log(res.tapIndex)
    //     let sourceType = 'camera'
    //     if (res.tapIndex == 0) {
    //       sourceType = 'camera'
    //     } else if (res.tapIndex == 1) {
    //       sourceType = 'album'
    //     }
    //     wx.chooseImage({
    //       count: 1,
    //       sizeType: ['original', 'compressed'],
    //       sourceType: [sourceType],
    //       success: function (res) {
    //          // tempFilePath可以作为img标签的src属性显示图片
    //         var tempFilePaths = res.tempFilePaths
    //         self.setData({ tempfilepath: tempFilePaths[0] }) //tempFilePaths为一个数组，显示数组第一个元素
    //        //setData 函数用于将数据从逻辑层发送到视图层（异步），同时改变对应的 this.data 的值（同步）。
    //       },
    //     })
    //   },
    // })
  // },
  //查询当前用户角色
  getUserRole(grade_id) {
    let openid = wx.getStorageSync('openid');
    wx.request({
      url: app.globalData.baseUrl + '/user/currentRole',
      data: { "openid": openid, "grade_id": grade_id },
      method: 'GET',
      success: function (res) {
        console.log("用户当前角色", res)
          if (res.data.code == 200&& res.data.data != undefined) {
            let roleId=res.data.data.role_id;
            app.globalData.roleId=roleId
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    self = this//没有这步编译不通过，self可以改成其他变量名
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.getGradeShow();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      "title": app.globalData.appName
    }
  }
})